import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ProductsItem} from "../shared/products-item";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent {
  @Input() products!: ProductsItem;

  @Output() addedProduct = new EventEmitter();

  onClickProduct() {
    this.addedProduct.emit();
  }
}
