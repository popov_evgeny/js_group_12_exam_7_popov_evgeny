import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ProductsItem} from "../shared/products-item";

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent {
  @Input() order!: ProductsItem;

  @Output() deleteProduct = new EventEmitter ();

  onClickBtnDelete() {
    this.deleteProduct.emit();
  }
}
