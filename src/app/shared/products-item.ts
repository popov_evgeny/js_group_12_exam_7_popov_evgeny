export class ProductsItem {
  constructor(
    public nameProduct: string,
    public productImage: string,
    public count: number,
    public cost: number,
    public newClass: string,
  ) {}
}
