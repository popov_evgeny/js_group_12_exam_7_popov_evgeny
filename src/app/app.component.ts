import {Component} from '@angular/core';
import {ProductsItem} from "./shared/products-item";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  greetingText = `Hello, order is empty! Please add some products!`;

  arrayProducts: ProductsItem [] = [
    new ProductsItem('Coffee', 'https://cdn3.iconfinder.com/data/icons/food-drink/512/Coffee_B-512.png', 0, 100, 'displayNone' ),
    new ProductsItem('Tea', 'https://pngimage.net/wp-content/uploads/2018/06/tea-icon-png-8.png', 0, 30, 'displayNone' ),
    new ProductsItem('Burger', 'https://cdn-icons-png.flaticon.com/512/161/161588.png', 0, 120, 'displayNone' ),
    new ProductsItem('Coca-Cola', 'https://cdn-icons-png.flaticon.com/512/1691/1691108.png', 0,55, 'displayNone' ),
    new ProductsItem('Samsa', 'https://cdn-icons-png.flaticon.com/512/4219/4219344.png', 0,65, 'displayNone' ),
    new ProductsItem('Shaurma', 'https://cdn-icons-png.flaticon.com/512/895/895238.png', 0, 110, 'displayNone' ),
    new ProductsItem('Hot-dog', 'https://spng.pngfind.com/pngs/s/55-553926_png-file-svg-hot-dog-icon-png-transparent.png ', 0, 90, 'displayNone' ),
    new ProductsItem('Sausage in dough', 'https://cdn5.vectorstock.com/i/1000x1000/45/54/sausage-roll-icon-simple-style-vector-7614554.jpg', 0, 60, 'displayNone' ),
  ]

  addProduct(index: number) {
    this.greetingText = 'Orders:';
    this.arrayProducts[index].newClass = 'displayBlock';
    this.arrayProducts[index].count++;
  }

  deleteProduct(index: number) {
    this.greetingText = `Hello, order is empty! Please add some products!`;
    this.arrayProducts[index].count = 0;
    if (this.arrayProducts[index].count === 0){
      this.arrayProducts[index].newClass = 'displayNone';
    }
  }

  getPrice() {
    return this.arrayProducts.reduce((acc, product) => {
      return acc + product.cost * product.count;
    }, 0);
  }
}
